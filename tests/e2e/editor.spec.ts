import { test, expect } from '@playwright/test';

test.describe('Editor', () => {
  test('Starts with example content', async ({ page }) => {
    await page.goto('http://localhost:3000/');

    await expect(page.locator('#editor')).toHaveValue(`# Title

# Slide 1

# Slide 2
`);
  });

  test('Content persists across page loads', async ({ page }) => {
    await page.goto('http://localhost:3000/');

    await page.locator('#editor').fill('# One\n\n# Two')
    await page.reload();

    await expect(page.locator('#editor')).toHaveValue('# One\n\n# Two');
  });

  test('Previous content is restored across reloads, even when url has no content', async ({ page }) => {
    await page.goto('http://localhost:3000/');

    await page.locator('#editor').fill('# One\n\n# Two')
    // no content query param here
    await page.goto('http://localhost:3000/');

    await expect(page.locator('#editor')).toHaveValue('# One\n\n# Two');
  });

  test('Marks slide when cursor moves into its content', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A');

    const titleSlide = page.locator('#slide-container .c-slide').nth(0);
    const slideA = page.locator('#slide-container .c-slide').nth(1);

    // Move cursor to title slide
    await page.locator('#editor').focus();
    await page.locator('#editor').press('ArrowUp');
    await page.locator('#editor').press('ArrowUp');

    await expect(titleSlide).toHaveAttribute('data-has-cursor', 'true');
    await expect(slideA).not.toHaveAttribute('data-has-cursor', 'true');

    // Arrow down to slide A
    await page.locator('#editor').press('ArrowDown');
    await page.locator('#editor').press('ArrowDown');

    await expect(titleSlide).not.toHaveAttribute('data-has-cursor', 'true');
    await expect(slideA).toHaveAttribute('data-has-cursor', 'true');
  });
});
