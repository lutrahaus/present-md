import { test, expect } from '@playwright/test';

/**
 * DOM features on slides that other parts of the app can rely on.
 */
test.describe('Slide API', () => {
  test('Has slide class', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    const slides = page.locator('#slide-container > *');

    await expect(slides).toHaveClass([/c-slide/, /c-slide/, /c-slide/, /c-slide/]);
  });

  test('Has data attribute for slide number', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    const slideA = page.locator('#slide-container .c-slide').nth(1);

    await expect(slideA).toHaveAttribute('data-slide-number', '2');
  });

  test('Has data attribute for char position', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    const slideA = page.locator('#slide-container .c-slide').nth(1);

    await expect(slideA).toHaveAttribute('data-ch-position', '9');
  });
});
