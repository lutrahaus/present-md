import { test, expect } from '@playwright/test';

test.describe('Slide content', () => {
  test('Renders one slide per h1', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await expect(page.locator('#slide-container .c-slide')).toHaveCount(4);
  });

  test('Uses h1 text as slide\'s title', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    const h1 = page.locator('#slide-container .c-slide').nth(1).locator('h1');
    await expect(h1).toHaveText('Slide A');
  });

  test('Does not render <h1> if it would be empty', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n#  ');

    const h1 = page.locator('#slide-container .c-slide').nth(1).locator('h1');
    await expect(h1).toHaveCount(0);
  });

  test('Converts md to html', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n- one\n- two\n- three\n');

    const ul = page.locator('#slide-container .c-slide').nth(0).locator('ul');
    await expect(ul.locator('li')).toHaveCount(3);
  });

  test('Renders links with target=_blank', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n[Example](https://example.org)');

    const link = page.locator('#slide-container .c-slide').nth(0).locator('a');
    await expect(link).toHaveAttribute('target', '_blank');
  });
});
