import { test, expect } from '@playwright/test';

const canReadClipboardText =
  (browserName: string) => !['firefox', 'webkit'].includes(browserName);

test.describe('Share', () => {
  test('User allow setting content via url', async ({ page }) => {
    page.on('dialog', dialog => dialog.accept());

    await page.goto('http://localhost:3000/?content=%23+First');
    await page.waitForTimeout(500);

    // verify editor value
    await expect(page.locator('#editor')).toHaveValue('# First');
  });

  test.describe('Share url added to clipboard', () => {
    test.beforeEach(({ context, browserName }) => {
      if (canReadClipboardText(browserName)) {
        // Grant clipboard r/w access
        context.grantPermissions(['clipboard-read', 'clipboard-write']);
      }
    });

    test('Share button copies url with content to clipboard', async ({ page, browserName }) => {
      test.skip(!canReadClipboardText(browserName), 'Browser does not support reading from clipboard programmatically');

      await page.goto('http://localhost:3000/');
      await page.locator('#editor').fill('# Title\n\nblah');

      let sawAlert = false;
      await page.on('dialog', dialog => {
        sawAlert = true;
        dialog.accept();
      });

      await page.getByRole('button', {
        name: 'Share'
      }).click();

      await expect(sawAlert).toBeTruthy();

      const url = await page.evaluate(() => navigator.clipboard.readText());
      await expect(url).toBe('http://localhost:3000/?content=%23+Title%0A%0Ablah');
    });

    test('Going to copied share url restores content', async ({ page, browserName }) => {
      test.skip(!canReadClipboardText(browserName), 'Browser does not support reading from clipboard programmatically');

      await page.goto('http://localhost:3000/');
      await page.locator('#editor').fill('# Title\n\nblah');

      let sawAlert = false;
      await page.on('dialog', dialog => {
        sawAlert = true;
        dialog.accept();
      });

      await page.getByRole('button', {
        name: 'Share'
      }).click();

      await expect(sawAlert).toBeTruthy();

      // Clear editor so the restored content definitely isn't from localstorage
      await page.locator('#editor').fill('');

      // Go to the copied url
      const url = await page.evaluate(() => navigator.clipboard.readText());
      await page.goto(url);

      // Verify editor value
      await expect(page.locator('#editor')).toHaveValue('# Title\n\nblah');
    });
  });
});
