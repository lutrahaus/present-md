import { test, expect } from '@playwright/test';

test.describe('Spellcheck', () => {
  test('Defaults to off', async ({ page }) => {
    await page.goto('http://localhost:3000/');

    await expect(page.locator('#editor')).not.toHaveAttribute('spellcheck', '');
    await expect(page.getByLabel('Spellcheck')).not.toBeChecked();
  });

  test('Toggles spellcheck on editor', async ({ page }) => {
    await page.goto('http://localhost:3000/');

    await expect(page.locator('#editor')).not.toHaveAttribute('spellcheck', '');
    await expect(page.getByLabel('Spellcheck')).not.toBeChecked();

    await page.getByLabel('Spellcheck').click({force: true});

    await expect(page.locator('#editor')).toHaveAttribute('spellcheck', 'true');
    await expect(page.getByLabel('Spellcheck')).toBeChecked();

    await page.getByLabel('Spellcheck').click({force: true});

    await expect(page.locator('#editor')).not.toHaveAttribute('spellcheck', '');
    await expect(page.getByLabel('Spellcheck')).not.toBeChecked();
  });

  test('Spellcheck setting persists across page loads', async ({ page }) => {
    await page.goto('http://localhost:3000/');

    await expect(page.locator('#editor')).not.toHaveAttribute('spellcheck', '');
    await expect(page.getByLabel('Spellcheck')).not.toBeChecked();

    await page.getByLabel('Spellcheck').click({force: true});

    await expect(page.locator('#editor')).toHaveAttribute('spellcheck', 'true');
    await expect(page.getByLabel('Spellcheck')).toBeChecked();

    await page.reload();

    await expect(page.locator('#editor')).toHaveAttribute('spellcheck', 'true');
    await expect(page.getByLabel('Spellcheck')).toBeChecked();
  });
});
