import { test, expect } from '@playwright/test';

test.describe('Frontmatter', () => {
  test.describe('Slideshow theme', () => {
    test('Defaults to light theme', async ({ page }) => {
      await page.goto('http://localhost:3000/');

      await page.locator('#editor').fill('# Title\n\n# Slide 1\n');

      await expect(page.locator('#slides-theme-link')).toHaveAttribute('data-theme-name', 'light');
      await expect(page.locator('#syntax-theme-link')).toHaveAttribute('data-theme-name', 'light');
    });

    test('Specify light theme', async ({ page }) => {
      await page.goto('http://localhost:3000/');

      await page.locator('#editor').fill('theme: light\n# Title\n\n# Slide 1\n');

      await expect(page.locator('#slides-theme-link')).toHaveAttribute('data-theme-name', 'light');
      await expect(page.locator('#syntax-theme-link')).toHaveAttribute('data-theme-name', 'light');
    });

    test('Specify dark theme', async ({ page }) => {
      await page.goto('http://localhost:3000/');

      await page.locator('#editor').fill('theme: dark\n# Title\n\n# Slide 1\n');

      await expect(page.locator('#slides-theme-link')).toHaveAttribute('data-theme-name', 'dark');
      await expect(page.locator('#syntax-theme-link')).toHaveAttribute('data-theme-name', 'dark');
    });

    test('Theme key/value are not case sensitive', async ({ page }) => {
      await page.goto('http://localhost:3000/');

      await page.locator('#editor').fill('theme: dark\n# Title\n\n# Slide 1\n');

      await expect(page.locator('#slides-theme-link')).toHaveAttribute('data-theme-name', 'dark');
      await expect(page.locator('#syntax-theme-link')).toHaveAttribute('data-theme-name', 'dark');

      await page.locator('#editor').fill('THEME: LIGHT\n# Title\n\n# Slide 1\n');

      await expect(page.locator('#slides-theme-link')).toHaveAttribute('data-theme-name', 'light');
      await expect(page.locator('#syntax-theme-link')).toHaveAttribute('data-theme-name', 'light');

      await page.locator('#editor').fill('Theme: Dark\n# Title\n\n# Slide 1\n');

      await expect(page.locator('#slides-theme-link')).toHaveAttribute('data-theme-name', 'dark');
      await expect(page.locator('#syntax-theme-link')).toHaveAttribute('data-theme-name', 'dark');
    });
  });

  test.describe('html flag', () => {
    test('When not specified, raw html is escaped', async ({ page }) => {
      await page.goto('http://localhost:3000/');

      await page.locator('#editor').fill('# Title\n\n<div class="raw-html">here</div>');

      const slide = page.locator('#slide-container .c-slide').nth(0);
      await expect(slide).toHaveText(/raw-html/);
      await expect(page.locator('#slide-container .raw-html')).toHaveCount(0);
    });

    test('When false, raw html is escaped', async ({ page }) => {
      await page.goto('http://localhost:3000/');

      await page.locator('#editor').fill('html: false\n\n# Title\n\n<div class="raw-html">here</div>');

      const slide = page.locator('#slide-container .c-slide').nth(0);
      await expect(slide).toHaveText(/raw-html/);
      await expect(page.locator('#slide-container .raw-html')).toHaveCount(0);
    });

    test('When true, raw html is rendered', async ({ page }) => {
      await page.goto('http://localhost:3000/');

      await page.locator('#editor').fill('html: true\n\n# Title\n\n<div class="raw-html">here</div>');

      const slide = page.locator('#slide-container .c-slide').nth(0);
      await expect(slide).not.toHaveText(/raw-html/);
      await expect(slide).toHaveText(/here/);
      await expect(page.locator('#slide-container .raw-html')).toHaveCount(1);
    });
  });
});
