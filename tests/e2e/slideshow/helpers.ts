import { Page } from '@playwright/test';

export async function checkIsFullscreen(page: Page, expected: boolean) {
  return await page.waitForFunction(
    (expected) => !!document.fullscreenElement === expected,
    expected
  );
}
