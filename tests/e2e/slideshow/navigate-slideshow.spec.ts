import { test, expect, Page } from '@playwright/test';

test.describe('Navigating slideshow @fullscreen', () => {
  test('Right arrow goes to next slide', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await page.getByRole('button', {
      name: 'Slideshow'
    }).click();

    const titleSlide = page.locator('#slide-container .c-slide').nth(0);
    const slideA = page.locator('#slide-container .c-slide').nth(1);

    await expect(titleSlide).toHaveAttribute('aria-current', 'true');

    await page.locator('#slide-container').press('ArrowRight');

    await expect(slideA).toHaveAttribute('aria-current', 'true');
  });

  test('Spacebar goes to next slide', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await page.getByRole('button', {
      name: 'Slideshow'
    }).click();

    const titleSlide = page.locator('#slide-container .c-slide').nth(0);
    const slideA = page.locator('#slide-container .c-slide').nth(1);

    await expect(titleSlide).toHaveAttribute('aria-current', 'true');

    await page.locator('#slide-container').press('Space');

    await expect(slideA).toHaveAttribute('aria-current', 'true');
  });

  test('Mouse click goes to next slide', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await page.getByRole('button', {
      name: 'Slideshow'
    }).click();

    const titleSlide = page.locator('#slide-container .c-slide').nth(0);
    const slideA = page.locator('#slide-container .c-slide').nth(1);

    await expect(titleSlide).toHaveAttribute('aria-current', 'true');

    await page.locator('#slide-container').click();

    await expect(slideA).toHaveAttribute('aria-current', 'true');
  });

  test.describe('With touch enabled', () => {
    test.use({ hasTouch: true });

    test('Tap goes to next slide', async ({ page }) => {
      await page.goto('http://localhost:3000/');
      await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

      await page.getByRole('button', {
        name: 'Slideshow'
      }).click();

      const titleSlide = page.locator('#slide-container .c-slide').nth(0);
      const slideA = page.locator('#slide-container .c-slide').nth(1);

      await expect(titleSlide).toHaveAttribute('aria-current', 'true');

      await page.locator('#slide-container').tap();

      await expect(slideA).toHaveAttribute('aria-current', 'true');
    });
  });

  test('Scrolling to the right goes to next slide', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await page.getByRole('button', {
      name: 'Slideshow'
    }).click();

    const titleSlide = page.locator('#slide-container .c-slide').nth(0);
    const slideA = page.locator('#slide-container .c-slide').nth(1);

    await expect(titleSlide).toHaveAttribute('aria-current', 'true');

    await horizontalScrollSlides(page, page.viewportSize()!.width / 4);

    await expect(slideA).toHaveAttribute('aria-current', 'true');
  });

  test('Left arrow goes to next slide', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await page.getByRole('button', {
      name: 'Slideshow'
    }).click();

    const titleSlide = page.locator('#slide-container .c-slide').nth(0);
    const slideA = page.locator('#slide-container .c-slide').nth(1);

    // Go right by one slide
    await titleSlide.click();

    await expect(slideA).toHaveAttribute('aria-current', 'true');

    await page.locator('#slide-container').press('ArrowLeft');

    await expect(titleSlide).toHaveAttribute('aria-current', 'true');
  });

  test('Scrolling left goes to previous slide', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await page.getByRole('button', {
      name: 'Slideshow'
    }).click();

    const titleSlide = page.locator('#slide-container .c-slide').nth(0);
    const slideA = page.locator('#slide-container .c-slide').nth(1);

    // Go right by one slide
    await titleSlide.click();

    await expect(slideA).toHaveAttribute('aria-current', 'true');

    await horizontalScrollSlides(page, page.viewportSize()!.width / 4 * -1);

    await expect(titleSlide).toHaveAttribute('aria-current', 'true');
  });
});

// TODO Find a cleaner way to scroll an element
async function horizontalScrollSlides(page: Page, xDelta: number) {
  return await page.waitForFunction((delta) => {
    document.getElementById('slide-container')?.scrollBy(delta, 0);
    return true;
  }, xDelta);
}
