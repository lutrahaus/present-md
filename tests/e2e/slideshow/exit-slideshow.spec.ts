import { test, expect } from '@playwright/test';
import { checkIsFullscreen } from './helpers.js';

test.describe('Exiting slideshow @fullscreen', () => {
  test('Esc exits slideshow', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await page.getByRole('button', {
      name: 'Slideshow'
    }).click();

    await checkIsFullscreen(page, true);

    await page.locator('#slide-container').press('Escape');

    await checkIsFullscreen(page, false);
    await expect(page.locator('#slide-container .c-slide[aria-current]')).toHaveCount(0);
  });
});
