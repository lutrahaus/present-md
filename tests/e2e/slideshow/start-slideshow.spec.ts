import { test, expect } from '@playwright/test';
import { checkIsFullscreen } from './helpers.js';

test.describe('Starting slideshow @fullscreen', () => {
  test('Clicking Slideshow button starts on first slide', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await checkIsFullscreen(page, false);

    await page.getByRole('button', {
      name: 'Slideshow'
    }).click();

    const titleSlide = page.locator('#slide-container .c-slide').nth(0);

    await checkIsFullscreen(page, true);
    await expect(titleSlide).toHaveAttribute('aria-current', 'true');
  });

  test('Clicking Slideshow button when no slides shows warning', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('');

    let sawAlert = false;
    await page.on('dialog', dialog => {
      sawAlert = true;
      dialog.accept();
    });

    await page.getByRole('button', {
      name: 'Slideshow'
    }).click();

    expect(sawAlert).toBeTruthy();
    await checkIsFullscreen(page, false);
  });

  test('Double clicking slide starts on that slide', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await checkIsFullscreen(page, false);

    const slideA = page.locator('#slide-container .c-slide').nth(1);

    // double click 2nd slide
    await slideA.dblclick();

    await checkIsFullscreen(page, true);
    await expect(slideA).toHaveAttribute('aria-current', 'true');
  });

  test('Alt + Enter starts on first slide', async ({ page }) => {
    await page.goto('http://localhost:3000/');
    await page.locator('#editor').fill('# Title\n\n# Slide A\n\n# Slide B\n\n# Slide C');

    await checkIsFullscreen(page, false);

    await page.locator('#editor').press('Alt+Enter');

    const titleSlide = page.locator('#slide-container .c-slide').nth(0);

    await checkIsFullscreen(page, true);
    await expect(titleSlide).toHaveAttribute('aria-current', 'true');
  });
});
