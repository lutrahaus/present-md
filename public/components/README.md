# What is this

This directory contains Prism language defs which the Prism Autoloader plugin
will load as needed.

https://prismjs.com/plugins/autoloader/#how-to-use
