# Goal

Quick-and-dirty slide deck generator. Probably not for epic keynotes, but more for when you need a deck *right now*.

# Basics

[x] Every h1 is a new slide. First slide is treated as title slide.

# Presentation mode

Present directly from the app

# Front matter

To possibly configure a few things. Ideally this is kept minimal.

- Theme: light, dark

# Exporting

- xlsx
- md
- [web share api](https://developer.mozilla.org/en-US/docs/Web/API/Web_Share_API)?
- pdf

# Importing

Any text file containing markdown should work?

# Other

- Save textarea's content in localstorage
- Syntax highlighting in code blocks
- Scroll preview with editor and vice versa
- PWA
