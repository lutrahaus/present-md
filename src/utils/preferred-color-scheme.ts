type PreferredScheme = 'dark' | 'light';

const mql = window.matchMedia('(prefers-color-scheme: dark)');

/**
 * Check preferred scheme.
 *
 * @returns Current preferred scheme
 */
export function preferredColorScheme(): PreferredScheme {
  return mql.matches ? 'dark' : 'light';
}

export type StopListeningCallback = () => void;
type SchemeChangeCallback = (scheme: PreferredScheme) => unknown;

/**
 * Listen for changes to preferred scheme.
*
* @returns Function that stops listening when called.
 */
export function onColorSchemeChange(cb: SchemeChangeCallback) {
  const listener = (event: MediaQueryListEvent) => {
    cb(event.matches ? 'dark' : 'light');
  };

  mql.addEventListener('change', listener);
  return () => mql.removeEventListener('change', listener);
}
