import { describe, expect, test } from 'vitest';
import { slugify } from './slug';

describe('slugify', () => {
  test('just letters', () => {
    const slug = slugify('abc');

    expect(slug).toBe('abc');
  });

  test('just numbers', () => {
    const slug = slugify('123');

    expect(slug).toBe('123');
  });

  test('just letters and numbers', () => {
    const slug = slugify('abc123');

    expect(slug).toBe('abc123');
  });

  test('just whitespace', () => {
    const slug = slugify(' ');

    expect(slug).toBe('');
  });

  test('leading special chars', () => {
    const slug = slugify('!ab');

    expect(slug).toBe('ab');
  });

  test('special chars in middle', () => {
    const slug = slugify('a!b');

    expect(slug).toBe('ab');
  });

  test('trailing special chars', () => {
    const slug = slugify('ab!');

    expect(slug).toBe('ab');
  });

  test('special chars at everywhere', () => {
    const slug = slugify('!a!b!');

    expect(slug).toBe('ab');
  });

  test('consecutive special chars', () => {
    const slug = slugify('a!!b');

    expect(slug).toBe('ab');
  });

  test('consecutive special chars everywhere', () => {
    const slug = slugify('!!a!!b!!');

    expect(slug).toBe('ab');
  });

  test('actual title 1', () => {
    const slug = slugify('JavaScript: The Good Parts');

    expect(slug).toBe('javascript-the-good-parts');
  });

  test('actual title 2', () => {
    const slug = slugify('Never do this!');

    expect(slug).toBe('never-do-this');
  });

  test('actual title 3', () => {
    const slug = slugify('Intro to web dev');

    expect(slug).toBe('intro-to-web-dev');
  });

  test('consecutive separator chars', () => {
    const slug = slugify('--this--is--a--test--');

    expect(slug).toBe('this-is-a-test');
  });
});
