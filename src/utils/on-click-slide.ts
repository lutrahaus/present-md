type Callback = (slide: HTMLElement) => unknown;
function createHandler(callback: Callback) {
  return (event: MouseEvent) => {
    if (event.target instanceof HTMLElement) {
      const slide = event.target.closest<HTMLElement>('.c-slide');
      if (slide) {
        callback(slide);
      }
    }
  };
}

export function onDoubleClickSlide(
  slideContainer: HTMLElement,
  callback: (slide: HTMLElement) => unknown
) {
  const handler = createHandler(callback);

  slideContainer.addEventListener('dblclick', handler);
  return () => slideContainer.removeEventListener('dblclick', handler);
}

export function onClickSlide(
  slideContainer: HTMLElement,
  callback: (slide: HTMLElement) => unknown
) {
  const handler = createHandler(callback);

  slideContainer.addEventListener('click', handler);
  return () => slideContainer.removeEventListener('click', handler);
}
