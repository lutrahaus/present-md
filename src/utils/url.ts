export function currentUrl(): URL {
  return new URL(document.location.toString());
}
