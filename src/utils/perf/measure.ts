import { mark, stop } from 'marky';

export function start(name: string) {
  if (import.meta.env.DEV) {
    mark(name);
  }
}

export function end(name: string) {
  if (import.meta.env.DEV) {
    stop(name);
  }
}
