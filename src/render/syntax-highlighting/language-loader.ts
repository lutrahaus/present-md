import { Prism } from './prism';

type Dependencies = string | Array<string>;

/**
 * language -> dependency languages
 */
var langDependencies: Record<string, Dependencies> = {
  "javascript": "clike",
  "actionscript": "javascript",
  "apex": [
    "clike",
    "sql"
  ],
  "arduino": "cpp",
  "aspnet": [
    "markup",
    "csharp"
  ],
  "birb": "clike",
  "bison": "c",
  "c": "clike",
  "csharp": "clike",
  "cpp": "c",
  "cfscript": "clike",
  "chaiscript": [
    "clike",
    "cpp"
  ],
  "cilkc": "c",
  "cilkcpp": "cpp",
  "coffeescript": "javascript",
  "crystal": "ruby",
  "css-extras": "css",
  "d": "clike",
  "dart": "clike",
  "django": "markup-templating",
  "ejs": [
    "javascript",
    "markup-templating"
  ],
  "etlua": [
    "lua",
    "markup-templating"
  ],
  "erb": [
    "ruby",
    "markup-templating"
  ],
  "fsharp": "clike",
  "firestore-security-rules": "clike",
  "flow": "javascript",
  "ftl": "markup-templating",
  "gml": "clike",
  "glsl": "c",
  "go": "clike",
  "gradle": "clike",
  "groovy": "clike",
  "haml": "ruby",
  "handlebars": "markup-templating",
  "haxe": "clike",
  "hlsl": "c",
  "idris": "haskell",
  "java": "clike",
  "javadoc": [
    "markup",
    "java",
    "javadoclike"
  ],
  "jolie": "clike",
  "jsdoc": [
    "javascript",
    "javadoclike",
    "typescript"
  ],
  "js-extras": "javascript",
  "json5": "json",
  "jsonp": "json",
  "js-templates": "javascript",
  "kotlin": "clike",
  "latte": [
    "clike",
    "markup-templating",
    "php"
  ],
  "less": "css",
  "lilypond": "scheme",
  "liquid": "markup-templating",
  "markdown": "markup",
  "markup-templating": "markup",
  "mongodb": "javascript",
  "n4js": "javascript",
  "objectivec": "c",
  "opencl": "c",
  "parser": "markup",
  "php": "markup-templating",
  "phpdoc": [
    "php",
    "javadoclike"
  ],
  "php-extras": "php",
  "plsql": "sql",
  "processing": "clike",
  "protobuf": "clike",
  "pug": [
    "markup",
    "javascript"
  ],
  "purebasic": "clike",
  "purescript": "haskell",
  "qsharp": "clike",
  "qml": "javascript",
  "qore": "clike",
  "racket": "scheme",
  "cshtml": [
    "markup",
    "csharp"
  ],
  "jsx": [
    "markup",
    "javascript"
  ],
  "tsx": [
    "jsx",
    "typescript"
  ],
  "reason": "clike",
  "ruby": "clike",
  "sass": "css",
  "scss": "css",
  "scala": "java",
  "shell-session": "bash",
  "smarty": "markup-templating",
  "solidity": "clike",
  "soy": "markup-templating",
  "sparql": "turtle",
  "sqf": "clike",
  "squirrel": "clike",
  "stata": [
    "mata",
    "java",
    "python"
  ],
  "t4-cs": [
    "t4-templating",
    "csharp"
  ],
  "t4-vb": [
    "t4-templating",
    "vbnet"
  ],
  "tap": "yaml",
  "tt2": [
    "clike",
    "markup-templating"
  ],
  "textile": "markup",
  "twig": "markup-templating",
  "typescript": "javascript",
  "v": "clike",
  "vala": "clike",
  "vbnet": "basic",
  "velocity": "markup",
  "wiki": "markup",
  "xeora": "markup",
  "xml-doc": "markup",
  "xquery": "markup"
};

/**
 * alias -> language
 */
var langAliases: Record<string, string> = {
  "html": "markup",
  "xml": "markup",
  "svg": "markup",
  "mathml": "markup",
  "ssml": "markup",
  "atom": "markup",
  "rss": "markup",
  "js": "javascript",
  "g4": "antlr4",
  "ino": "arduino",
  "arm-asm": "armasm",
  "art": "arturo",
  "adoc": "asciidoc",
  "avs": "avisynth",
  "avdl": "avro-idl",
  "gawk": "awk",
  "sh": "bash",
  "shell": "bash",
  "shortcode": "bbcode",
  "rbnf": "bnf",
  "oscript": "bsl",
  "cs": "csharp",
  "dotnet": "csharp",
  "cfc": "cfscript",
  "cilk-c": "cilkc",
  "cilk-cpp": "cilkcpp",
  "cilk": "cilkcpp",
  "coffee": "coffeescript",
  "conc": "concurnas",
  "jinja2": "django",
  "dns-zone": "dns-zone-file",
  "dockerfile": "docker",
  "gv": "dot",
  "eta": "ejs",
  "xlsx": "excel-formula",
  "xls": "excel-formula",
  "gamemakerlanguage": "gml",
  "po": "gettext",
  "gni": "gn",
  "ld": "linker-script",
  "go-mod": "go-module",
  "hbs": "handlebars",
  "mustache": "handlebars",
  "hs": "haskell",
  "idr": "idris",
  "gitignore": "ignore",
  "hgignore": "ignore",
  "npmignore": "ignore",
  "webmanifest": "json",
  "kt": "kotlin",
  "kts": "kotlin",
  "kum": "kumir",
  "tex": "latex",
  "context": "latex",
  "ly": "lilypond",
  "emacs": "lisp",
  "elisp": "lisp",
  "emacs-lisp": "lisp",
  "md": "markdown",
  "moon": "moonscript",
  "n4jsd": "n4js",
  "nani": "naniscript",
  "objc": "objectivec",
  "qasm": "openqasm",
  "objectpascal": "pascal",
  "px": "pcaxis",
  "pcode": "peoplecode",
  "plantuml": "plant-uml",
  "pq": "powerquery",
  "mscript": "powerquery",
  "pbfasm": "purebasic",
  "purs": "purescript",
  "py": "python",
  "qs": "qsharp",
  "rkt": "racket",
  "razor": "cshtml",
  "rpy": "renpy",
  "res": "rescript",
  "robot": "robotframework",
  "rb": "ruby",
  "sh-session": "shell-session",
  "shellsession": "shell-session",
  "smlnj": "sml",
  "sol": "solidity",
  "sln": "solution-file",
  "rq": "sparql",
  "sclang": "supercollider",
  "t4": "t4-cs",
  "trickle": "tremor",
  "troy": "tremor",
  "trig": "turtle",
  "ts": "typescript",
  "tsconfig": "typoscript",
  "uscript": "unrealscript",
  "uc": "unrealscript",
  "url": "uri",
  "vb": "visual-basic",
  "vba": "visual-basic",
  "webidl": "web-idl",
  "mathematica": "wolfram",
  "nb": "wolfram",
  "wl": "wolfram",
  "xeoracube": "xeora",
  "yml": "yaml",
  "clj": "clojure",
  "cljs": "clojure",
  "cljc": "clojure",
  "edn": "clojure",
};

type LangDataItem = {
  callbacks: Array<{
    success?: () => void;
    error?: () => void;
  }>;
  error: boolean;
  /**
   * - true -> currently loading
   * - false -> done with load attempt (check error flag for outcome)
   * - undefined -> loading not started
   */
  loading?: boolean;
};

var langData: Record<string, LangDataItem> = {};
var languagesPath = 'components/';

var config = {
  languages_path: languagesPath,
  use_minified: true,
  loadLanguages: loadLanguages
};

/**
 * Lazily loads an external script.
 *
 * @param {string} src
 * @param {() => void} [success]
 * @param {() => void} [error]
 */
function addScript(src: string, success?: () => void, error?: () => void) {
  var s = document.createElement('script');
  s.src = src;
  s.async = true;
  s.onload = function () {
    document.body.removeChild(s);
    success && success();
  };
  s.onerror = function () {
    document.body.removeChild(s);
    error && error();
  };
  document.body.appendChild(s);
}

/**
 * Returns whether the given language is currently loaded.
 */
export function isLoaded(prism: Prism, lang: string): boolean {
  if (lang.indexOf('!') >= 0) {
    // forced reload
    return false;
  }

  lang = langAliases[lang] || lang; // resolve alias

  if (lang in prism.languages) {
    // the given language is already loaded
    return true;
  }

  // this will catch extensions like CSS extras that don't add a grammar to Prism.languages
  var data = langData[lang];
  return data && !data.error && data.loading === false;
}

/**
 * Returns the path to a grammar, using the language_path and use_minified config keys.
 *
 * @param {string} lang
 * @returns {string}
 */
function getLanguagePath(lang: string) {
  return config.languages_path + 'prism-' + lang + (config.use_minified ? '.min' : '') + '.js';
}

/**
 * Loads all given grammars concurrently.
 *
 * @param langs
 * @param success Optional
 * @param error Optional. This callback will be invoked on the first language to fail.
 */
function loadLanguages(
  prism: Prism,
  langs: string | Array<string>,
  success?: (langs: Array<string>) => void,
  error?: (lang: string) => void
) {
  const languages = typeof langs === 'string' ? [langs] : langs;

  var total = languages.length;
  var completed = 0;
  var failed = false;

  if (total === 0) {
    if (success) {
      setTimeout(success, 0);
    }
    return;
  }

  function successCallback() {
    if (failed) {
      return;
    }
    completed++;
    if (completed === total) {
      success && success(languages);
    }
  }

  languages.forEach(function(language) {
    loadLanguage(prism, language, successCallback, function() {
      if (failed) {
        return;
      }
      failed = true;
      error && error(language);
    });
  });
}

/**
 * Loads a grammar with its dependencies.
 */
function loadLanguage(prism: Prism, lang: string, success?: () => void, error?: () => void) {
  var force = lang.indexOf('!') >= 0;

  lang = lang.replace('!', '');
  lang = langAliases[lang] || lang;

  function load(prism: Prism) {
    var data = langData[lang];
    if (!data) {
      data = langData[lang] = {
        callbacks: [],
        error: false,
        loading: undefined,
      };
    }
    data.callbacks.push({
      success: success,
      error: error
    });

    if (!force && isLoaded(prism, lang)) {
      // the language is already loaded and we aren't forced to reload
      languageCallback(lang, 'success');
    } else if (!force && data.error) {
      // the language failed to load before and we don't reload
      languageCallback(lang, 'error');
    } else if (force || !data.loading) {
      // the language isn't currently loading and/or we are forced to reload
      data.loading = true;
      data.error = false;

      addScript(getLanguagePath(lang), function () {
        data.loading = false;
        languageCallback(lang, 'success');

      }, function () {
        data.loading = false;
        data.error = true;
        languageCallback(lang, 'error');
      });
    }
  }

  var dependencies = langDependencies[lang];
  if (dependencies && dependencies.length) {
    loadLanguages(prism, dependencies, () => load(prism), error);
  } else {
    load(prism);
  }
}

/**
 * Runs all callbacks of the given type for the given language.
 */
function languageCallback(lang: string, type: 'success' | 'error') {
  if (langData[lang]) {
    var callbacks = langData[lang].callbacks;
    for (var i = 0, l = callbacks.length; i < l; i++) {
      var callback = callbacks[i][type];
      if (callback) {
        setTimeout(callback, 0);
      }
    }
    callbacks.length = 0;
  }
}

export function loadSupportFor(prism: Prism, lang: string): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    loadLanguages(
      prism,
      lang,
      () => resolve(),
      (badLang) => reject(new Error(`Could not load ${badLang}`))
    );
  });
}
