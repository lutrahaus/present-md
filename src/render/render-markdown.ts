import { Remarkable } from 'remarkable';
import { linkify } from 'remarkable/linkify';
import { prismFriendlyFence } from './prism-friendly-fence';
import { omitEmptyH1 } from './omit-empty-h1';
import { loadSupportFor } from './syntax-highlighting/language-loader';
import {
  hasLoadedLanguageHighlighter,
  highlight,
  highlightAll,
  isHighlightableLanguage,
  loadPrism
} from './syntax-highlighting/prism';

const baseOptions: Remarkable.Options = {
  linkTarget: '_blank',
  highlight(code, lang) {
    if (hasLoadedLanguageHighlighter(lang)) {
      try {
        const htmlString = highlight(code, lang);
        return htmlString ? htmlString : '';
      } catch (err) {
        console.error(err);
      }
    } else if (isHighlightableLanguage(lang)) {
      // Kick off async side effect that loads the code needed to highlight
      // this language and then does a bulk highlight on everything
      loadPrism()
        .then(prism => loadSupportFor(prism, lang))
        .then(() => highlightAll())
        .catch(err => console.error(err));
    }

    return '';
  }
};

const defaultRemarkable = new Remarkable(baseOptions)
  // Convert bare urls into links
  .use(linkify)
  .use(prismFriendlyFence)
  .use(omitEmptyH1);

const htmlRemarkable = new Remarkable({
  ...baseOptions,
  html: true
})
  // Convert bare urls into links
  .use(linkify)
  .use(prismFriendlyFence)
  .use(omitEmptyH1);

/**
 * md -> html
 *
 * @param md markdown
 * @param renderHtml true if html in md will be rendered, false to escape it
 * @returns html
 */
export function renderMarkdown(md: string, renderHtml: boolean): string {
  return renderHtml ? htmlRemarkable.render(md) : defaultRemarkable.render(md);
}
