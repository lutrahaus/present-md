import { Remarkable } from 'remarkable';

/**
 * Prism plugin that converts fence render result from
 *
 * ```
 * <pre>
 *   <code class="language-x">
 *    ...
 *   </code>
 * </pre>
 * ```
 *
 * to
 *
 * ```
 * <pre class="language-x">
 *   <code class="language-x">
 *     ...
 *   </code>
 * </pre>
 * ```
 */
export const prismFriendlyFence: Remarkable.Plugin = (md) => {
  const original = md.renderer.rules.fence;

  md.renderer.rules.fence = function(tokens, idx, options, env, instance) {
    const prefix = options?.langPrefix ? options.langPrefix : '';
    const langName = tokens[idx].params;
    const langClass = langName ? ` class="${prefix + langName}"` : '';

    return original.call(this, tokens, idx, options, env, instance)
      .replace('<pre>', `<pre${langClass}>`);
  };
};
