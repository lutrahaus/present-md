import { Deck, Slide } from '../deck';
import {
  blankSlideElement,
  setChPosition,
  setContent,
  setSlideNumber,
} from '../slide';
import { renderMarkdown } from './render-markdown';

/**
 * Description of a slide.
*/
type SlideSpec = {
  chPosition: number;
  slideNumber: number;
  content: string;
};

/**
 * Specs of the slides from previous rendering pass.
 */
let previousSpecs: Array<SlideSpec> = [];

export function renderSlides(deck: Deck, slideContainer: HTMLElement) {
  const slideSpecs: Array<SlideSpec> = deck.slides.map(toSlideSpec);
  const existingElements = Array.from(slideContainer.children);

  slideSpecs.forEach((spec, index) => {
    const existingElement = existingElements[index];

    // Slide element exists at this index
    if (existingElement instanceof HTMLElement) {
      // Update chPosition if changed
      if (previousSpecs[index] && previousSpecs[index].chPosition !== spec.chPosition) {
        setChPosition(existingElement, spec.chPosition);
      }

      // Update slideNumber if changed
      if (previousSpecs[index] && previousSpecs[index].slideNumber !== spec.slideNumber) {
        setSlideNumber(existingElement, spec.slideNumber);
      }

      // Update content if changed
      if (previousSpecs[index] && previousSpecs[index].content !== spec.content) {
        setContent(existingElement, spec.content);
      }
    } else {
      // No element at this index yet. Create element and add it to dom.
      const newElement = blankSlideElement();

      setChPosition(newElement, spec.chPosition);
      setSlideNumber(newElement, spec.slideNumber);
      setContent(newElement, spec.content);

      slideContainer.appendChild(newElement);
    }
  });

  // More elements than slides, trim the unneeded elements
  if (existingElements.length > slideSpecs.length) {
    existingElements.slice(slideSpecs.length).forEach(node => node.remove());
  }

  // Remember specs for next time through
  previousSpecs = slideSpecs;
}

function toSlideSpec(slide: Slide, index: number): SlideSpec {
  const md = slide.lines.map(slide => slide.content).join('\n');
  const html = renderMarkdown(md, slide.embeddedHtml === 'render');

  return {
    chPosition: slide.chPosition,
    slideNumber: index + 1,
    content: html,
  };
}
