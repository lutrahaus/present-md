import { Remarkable } from 'remarkable';
import { HeadingToken } from 'remarkable/lib';

/**
 * Omits `h1` if it will not have text content.
 */
export const omitEmptyH1: Remarkable.Plugin = (md) => {
  const shouldRender = (firstToken?: HeadingToken, secondToken?: HeadingToken) => {
    const isH1 =  firstToken?.hLevel === 1 && secondToken?.hLevel === 1;
    const isEmpty = firstToken?.type === 'heading_open' && secondToken?.type === 'heading_close';
    return !isH1 || !isEmpty;
  };

  const originalHeadingOpen = md.renderer.rules.heading_open;
  md.renderer.rules.heading_open = function(tokens, index, options, env, instance) {
    const currentToken = tokens[index] as HeadingToken;
    const nextToken = tokens[index + 1] as HeadingToken | undefined;

    if (shouldRender(currentToken, nextToken)) {
      return originalHeadingOpen(tokens, index, options, env, instance);
    } else {
      return '';
    }
  };

  const originalHeadingClose = md.renderer.rules.heading_close;
  md.renderer.rules.heading_close = function(tokens, index, options, env, instance) {
    const prevToken = tokens[index - 1] as HeadingToken | undefined;
    const currentToken = tokens[index] as HeadingToken;

    if (shouldRender(prevToken, currentToken)) {
      return originalHeadingClose(tokens, index, options, env, instance);
    } else {
      return '';
    }
  };
};
