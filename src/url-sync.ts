import { currentUrl } from "./utils/url";

const CONTENT_QUERY_PARAM = 'content';

// Sync history with content
export function syncUrl(md: string, isNew: boolean) {
  if (isNew) {
    startNewPresentation(md);
  } else {
    updateCurrentPresentation(md);
  }
}

function startNewPresentation(md: string) {
  const url = addContentToUrl(currentUrl(), md);
  window.history.pushState({}, '', url);
}

function updateCurrentPresentation(md: string) {
  const url = addContentToUrl(currentUrl(), md);
  window.history.replaceState({}, '', url);
}

/**
 * @param current
 * @param md
 * @returns Copy of `current` with `md` added
 */
export function addContentToUrl(current: URL, md: string): URL {
  const url = new URL(current);
  url.searchParams.set(CONTENT_QUERY_PARAM, md);
  return url;
}

/**
 * Get markdown from a url.
 *
 * @param url
 * @returns md string or null if there is no md
 */
export function contentFromUrl(url: URL): string | null {
  return url.searchParams.get(CONTENT_QUERY_PARAM);
}
