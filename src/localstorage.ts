const MARKDOWN_KEY = 'present-md.markdown';
const SPELLCHECK_KEY = 'present-md.spellcheck';
const SUBSEQUENT_VISIT = 'present-md.subsequent-visit';

export function getMarkdown() {
  return get(MARKDOWN_KEY);
}

export function getSpellcheck() {
  return get(SPELLCHECK_KEY) === 'true';
}

export function isFirstVisit() {
  return get(SUBSEQUENT_VISIT) !== 'true';
}

function get(key: string) {
  return window.localStorage.getItem(key);
}

export function setMarkdown(md: string) {
  return set(MARKDOWN_KEY, md);
}

export function setSpellcheck(on: boolean) {
  return set(SPELLCHECK_KEY, String(on));
}

export function markFirstVisit() {
  return set(SUBSEQUENT_VISIT, 'true');
}

function set(key: string, value: string) {
  window.localStorage.setItem(key, value);
}
