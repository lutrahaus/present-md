import { isFirstVisit, getMarkdown } from './localstorage';
import { contentFromUrl } from './url-sync';
import { currentUrl } from './utils/url';

export function initialMarkdown(): string {
  return contentFromUrl(currentUrl())
    || getMarkdown()
    || (isFirstVisit() && exampleTemplate()) 
    || '';
}

function exampleTemplate() {
  return  `# Title

# Slide 1

# Slide 2
`;
}
