import { Deck } from '../deck';
import { parseDeck } from '../parse/parse-deck';
import { getHasCursor, setHasCursor } from '../slide';

type OnCursorEnter = (slideElement: HTMLElement) => void;

/**
 * Wire "mark cursor slide" up to the dom.
 */
export function create(editor: HTMLTextAreaElement, slideContainer: HTMLElement, onCursorEnter: OnCursorEnter) {
  function markCursorSlide() {
    const markIt = () => {
      const deck = parseDeck(editor.value);
      const slideIndex = cursorSlideIndex(deck, editor.selectionStart);

      slideContainer.querySelectorAll<HTMLElement>(`.c-slide`)
        .forEach((slideElement, index) => {
          const hadCursorBefore = getHasCursor(slideElement);
          const hasCursorNow = index === slideIndex;

          if (!hadCursorBefore && hasCursorNow) {
            // cursor entered
            setHasCursor(slideElement, true);
            onCursorEnter(slideElement);
          } else if (hadCursorBefore && !hasCursorNow) {
            // cursor exited
            setHasCursor(slideElement, false);
          }
      });
    };

    // For some reason the values of `selectionStart` are weird when reading them
    // in handler for event that moved the cursor, so defer til later.
    setTimeout(markIt, 0);
  }

  const events = [
    // Arrows
    'keyup',
    // Click down
    'mousedown',
    // Mobile
    'touchstart',
    // Other input events
    'input',
    // Clipboard actions
    'paste',
    'cut',
    // Some browsers support this event
    'select',
    // Some browsers support this event
    'selectstart',
    // Selection, dragging text
    // 'mousemove',
  ];

  events.forEach(event => editor.addEventListener(event, markCursorSlide));
  return () => events.forEach(event => editor.removeEventListener(event, markCursorSlide));
}

/**
 * @param cursorChPosition Zero-based cursor position
 * @returns Zero-based index of slide that contains cursor
 */
function cursorSlideIndex(deck: Deck, cursorChPosition: number) {
  return deck.slides.reduce((index, slide, slideIndex) => {
    return cursorChPosition >= slide.chPosition
      ? slideIndex
      : index;
  }, 0);
}
