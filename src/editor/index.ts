import { create as trackCursorSlide } from './track-cursor-slide';
import { create as createMoveCursorToSlide } from './move-cursor-to-slide';
import { onClickSlide } from '../utils/on-click-slide';
import { create as trackVisibleSlides } from './track-visible-slides';
import { scrollIntoView } from '../slide';

/**
 * What caused the md content to change.
 */
export type ChangeSource = 'initial' | 'user' | 'file' | 'url';
type ContentChangeListener = (md: string, source: ChangeSource) => void;

export function initialize(
  editor: HTMLTextAreaElement,
  slideContainer: HTMLElement,
  initialMarkdown: string,
  onContentChange: ContentChangeListener
) {
  // Track what slides are full visible
  const { isFullyVisible } = trackVisibleSlides(slideContainer);

  // Track which slide has the cursor in its markdown
  trackCursorSlide(editor, slideContainer, cursorSlide => {
    // Ensure that the slide with cursor is visible
    if (!isFullyVisible(cursorSlide)) {
      scrollIntoView(cursorSlide, {
        // Force no-scroll because cursor can make big jumps
        behavior: 'auto',
        inline: 'center',
      });
    }
  });

  // Move cursor to slide's markdown when its preview slide is clicked
  const moveCursorToSlide = createMoveCursorToSlide(editor);
  onClickSlide(slideContainer, slide => moveCursorToSlide(slide));

  // set initial value
  setValue(initialMarkdown, 'initial');

  // update on input
  editor.addEventListener('input', (event) => {
    if (event.target instanceof HTMLTextAreaElement) {
      onContentChange(event.target.value, 'user');
    }
  });

  function setValue(md: string, source: ChangeSource) {
    editor.value = md;
    onContentChange(md, source);
  }

  function overwriteValue(md: string, sourceName: string, source: ChangeSource) {
    if (editor.value && md !== editor.value) {
      const overwrite = confirm(`Markdown from ${sourceName} will overwrite the current presentation.`);
      if (overwrite) {
        setValue(md, source);
      }
    }
  }

  return {
    overwriteValue,
    setValue,
  };
}
