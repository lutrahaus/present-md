import { getChPosition } from '../slide';

export function create(editor: HTMLTextAreaElement) {
  /**
   * @param chPosition Zero-based character position to move cursor to
   */
  function moveCursorTo(chPosition: number) {
    editor.focus();
    editor.setSelectionRange(chPosition, chPosition);
  }

  return (slide: HTMLElement) => {
    const chPosition = getChPosition(slide);

    if (chPosition != null) {
      moveCursorTo(chPosition);
    }
  };
}
