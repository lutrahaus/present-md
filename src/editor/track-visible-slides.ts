export function create(slideContainer: HTMLElement) {
  const fullyVisibleSlides = new Set<HTMLElement>();
  const fullyVisible = (el: HTMLElement) => fullyVisibleSlides.add(el);
  const notFullyVisible = (el: HTMLElement) => fullyVisibleSlides.delete(el);
  const isFullyVisible = (el: HTMLElement) => fullyVisibleSlides.has(el);

  const fullVisibilityThreshold = 1;
  const i = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (entry.intersectionRatio === fullVisibilityThreshold) {
        if (entry.target instanceof HTMLElement) {
          fullyVisible(entry.target);
        }
      } else {
        if (entry.target instanceof HTMLElement) {
          notFullyVisible(entry.target);
        }
      }
    });
  }, {
    root: slideContainer,
    threshold: fullVisibilityThreshold,
  });

  const m = new MutationObserver(records => {
    records.forEach(rec => {
      if (rec.type === 'childList') {
        rec.addedNodes.forEach(added => {
          if (added instanceof HTMLElement) {
            i.observe(added)
          }
        });

        rec.removedNodes.forEach(removed => {
          if (removed instanceof HTMLElement) {
            notFullyVisible(removed);
            i.unobserve(removed)
          }
        });
      }
    });
  });

  m.observe(slideContainer, {
    childList: true
  });

  return {
    isFullyVisible,
    cleanUp() {
      m.disconnect();
      i.disconnect();
    },
  };
}
