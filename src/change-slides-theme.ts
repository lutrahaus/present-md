import {
  onColorSchemeChange,
  preferredColorScheme,
  StopListeningCallback,
} from './utils/preferred-color-scheme';
import { Theme } from './frontmatter';

/**
 * Theme changer function.
 */
export type UpdateTheme = (newTheme: Theme) => void;

export function themeChanger(baseUrl: string, slideContainer: HTMLElement, slidesThemeLink: HTMLLinkElement, syntaxThemeLink: HTMLLinkElement): UpdateTheme {
  const urlsByTheme = new Map<Theme, {slides: string, syntax: string}>(
    [
      ['dark', {
        slides: baseUrl + 'slide-themes/dark/slides.css',
        syntax: baseUrl + 'slide-themes/dark/syntax.css'
      }],
      ['light', {
        slides: baseUrl + 'slide-themes/light/slides.css',
        syntax: baseUrl + 'slide-themes/light/syntax.css'
      }],
    ]
  );

  const renderTheme = (theme: Theme) => {
    const actualTheme = resolveConcreteTheme(theme);

    slideContainer.classList.remove('c-slides--has-theme');

    if (urlsByTheme.has(actualTheme)) {
      slideContainer.classList.add('c-slides--has-theme');

      // slides
      slidesThemeLink.dataset.themeName = actualTheme;
      slidesThemeLink.href = urlsByTheme.get(actualTheme)!.slides;

      // syntax
      syntaxThemeLink.dataset.themeName = actualTheme;
      syntaxThemeLink.href = urlsByTheme.get(actualTheme)!.syntax;
    }
  };

  let currentTheme: Theme | null = null;
  let stopListening: StopListeningCallback | null = null;

  return function changeTheme(newTheme: Theme) {
    if (newTheme !== currentTheme) {
      currentTheme = newTheme;

      if (newTheme === 'system') {
        // Start listening to system preference changes
        stopListening = onColorSchemeChange(scheme => renderTheme(scheme));
      } else if (stopListening) {
        // Stop listening to system preference changes
        stopListening();
        stopListening = null;
      }

      renderTheme(newTheme);
    }
  };
}

/**
 * @returns Any theme *except for* system
 */
function resolveConcreteTheme(theme: Theme): Exclude<Theme, 'system'> {
  return theme === 'system'
    ? preferredColorScheme()
    : theme;
}
