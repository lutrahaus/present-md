import { Frontmatter } from './frontmatter';
import { slugify } from './utils/slug';

export type Line = {
  content: string,
  lineLength: number,
  /** Zero-based position of first character of line */
  chPosition: number;
}

export type Slide = {
  /** Text content of the h1 of this slide, this may be blank */
  title: string;
  /** Lines of markdown that make up this slide */
  lines: Array<Line>;
  /** Zero-based position of first character of slide */
  chPosition: number;
  /** How embedded html will be treated */
  embeddedHtml: 'render' | 'escape';
}

export type Deck = {
  frontmatter: Frontmatter;
  slides: Array<Slide>;
}

export function title(deck: Deck, fallback = 'untitled'): string {
  return (deck.slides.length > 0 && deck.slides[0].title) || fallback;
}

/**
 * @returns Suggested filename for this deck.
 */
export function filename(deck: Deck): string {
  return `${slugify(title(deck))}.md`;
}
