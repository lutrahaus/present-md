export function save(filename: string, markdown: string) {
  const file = new File([markdown], filename, {
    type: 'text/markdown',
  });

  localDownload(file);
}

function localDownload(file: File) {
  const url = URL.createObjectURL(file);
  const link = document.createElement('a');

  link.href = url;
  link.download = file.name;

  document.body.appendChild(link);
  link.click();

  setTimeout(() => {
    link.remove();
    URL.revokeObjectURL(url);
  }, 5 * 1000);
}