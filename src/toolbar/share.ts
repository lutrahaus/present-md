import { addContentToUrl } from '../url-sync';
import { currentUrl } from '../utils/url';

export function share(md: string) {
  return navigator.clipboard.writeText(addContentToUrl(currentUrl(), md).toString())
    .then(() => alert('Shareable URL copied to clipboard'));
}
