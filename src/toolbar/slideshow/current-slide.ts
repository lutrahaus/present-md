// Helpers for dealing with current slide of presentation

export function getCurrent(slideContainer: HTMLElement) {
  return slideContainer.querySelector<HTMLElement>('[aria-current="true"]');
}

export function forgetCurrentSlide(slideContainer: HTMLElement) {
  const current = getCurrent(slideContainer);
  if (current) {
    markAsNotCurrent(current);
  }
}

function markAsCurrent(slide: HTMLElement) {
  slide.setAttribute('aria-current', 'true');
}

function markAsNotCurrent(slide: HTMLElement) {
  slide.removeAttribute('aria-current');
}

export function trackCurrentSlide(slideContainer: HTMLElement) {
  // Slide has to overlap slide container by at least this % to be "current"
  const overlapThreshold = 0.51;

  const observer = new IntersectionObserver((entries) => {
    entries.forEach(entry => {
      if (entry.intersectionRatio >= overlapThreshold) {
        if (entry.target instanceof HTMLElement) {
          // This is now the current slide, mark it
          markAsCurrent(entry.target);
        }
      } else {
        if (entry.target instanceof HTMLElement) {
          // No longer current, unmark it
          markAsNotCurrent(entry.target)
        }
      }
    });
  }, {
    root: slideContainer,
    threshold: overlapThreshold
  });

  // Watch all slides
  slideContainer.querySelectorAll('.c-slide').forEach(slide => observer.observe(slide));

  return observer;
}
