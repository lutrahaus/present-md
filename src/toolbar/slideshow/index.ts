import { Announcer } from '../../announcer';
import { scrollIntoView } from '../../slide';
import {
  getCurrent,
  forgetCurrentSlide,
  trackCurrentSlide,
} from './current-slide';

/**
 * Placeholder value that represents going off the end of the presentation.
 */
const NO_SLIDE = 'no-slide';

export function create(main: HTMLElement, slideContainer: HTMLElement, announcer: Announcer) {
  const indicatorClass = 'js-is-presenting';

  const changeCurrentTo = changeCurrentSlideTo(slideContainer);
  const changeCurrentBy = changeCurrentSlideBy(slideContainer);
  const nextSlide = changeCurrentBy.bind(null, 1);
  const previousSlide = changeCurrentBy.bind(null, -1);

  const handleKeyDown = keyDownHandler({nextSlide, previousSlide, exit: stop});
  const handleFullscreenChange = () => !document.fullscreenElement && stop();
  const handlePointerDown = onPointerDown(slideContainer, nextSlide);

  let previousFocusedElement: Element | null;
  let currentSlideTracker: IntersectionObserver | null = null;

  /**
   * @param startingSlideNumber 1-based slide number to start at. Defaults to
   * the first slide.
   */
  function start(startingSlideNumber = 1) {
    if (!hasSlides(slideContainer)) {
      alert('Create at least one slide to view the slideshow');
      return;
    }

    if (!isPresenting()) {
      // Add presentation mode sentinel class
      main.classList.add(indicatorClass);

      // Notify
      announcer.announcePolite('Slideshow started');

      // Turn on presentation styles
      slideContainer.classList.add('c-slides--presenting')

      // Go into fullscreen
      slideContainer.requestFullscreen()
        .catch((e) => {
          console.error('Cannot go fullscreen', e);
        })
        .finally(() => {
          // Move focus to slides
          previousFocusedElement = document.activeElement;
          slideContainer.tabIndex = -1;
          slideContainer.focus();

          // Listen for user interaction
          slideContainer.addEventListener('pointerdown', handlePointerDown);
          document.addEventListener('keydown', handleKeyDown);
          document.addEventListener('fullscreenchange', handleFullscreenChange);

          // Mark current slide as it scrolls into view
          currentSlideTracker = trackCurrentSlide(slideContainer);

          changeCurrentTo(startingSlideNumber);
        });
    }
  }

  function stop() {
    if (isPresenting()) {
      // Unmark current slide
      forgetCurrentSlide(slideContainer);

      // Clean up intersection observer
      currentSlideTracker?.disconnect();
      currentSlideTracker = null;

      // Remove interaction listeners
      slideContainer.removeEventListener('pointerdown', handlePointerDown);
      document.removeEventListener('keydown', handleKeyDown);
      document.removeEventListener('fullscreenchange', handleFullscreenChange);

      // Back to normal window view
      document.exitFullscreen()
        .catch(_ignored => {})
        .finally(() => {
          // Restore focus
          if (previousFocusedElement instanceof HTMLElement) {
            previousFocusedElement.focus();
            previousFocusedElement = null;
          }

          // Notify
          announcer.announcePolite('Slideshow stopped');

          // Remove sentinel class
          main.classList.remove(indicatorClass);

          // Turn off presentation styles
          slideContainer.classList.remove('c-slides--presenting')
        });
    }
  }

  function isPresenting() {
    return main.classList.contains(indicatorClass);
  }

  return {
    start,
  };
}

function hasSlides(slideContainer: HTMLElement) {
  return !!slideContainer.querySelector('.c-slide');
}

/**
 * Walk `<delta>` siblings from a slide and return the slide found, if any.
 *
 * @param currentSlide Starting point
 * @param delta How many sibling steps to take. Positive goes in the "next"
 * direction and negative goes in the "previous" direction.
 */
function findSibling(currentSlide: HTMLElement, delta: number): HTMLElement | typeof NO_SLIDE {
  const direction = delta > 0 ? 'nextElementSibling' : 'previousElementSibling';
  let stepsRemaining = Math.abs(delta);
  let sibling = currentSlide;

  while (stepsRemaining > 0) {
    if (sibling[direction]?.matches('.c-slide')) {
      sibling = sibling[direction] as HTMLElement;
    } else {
      return NO_SLIDE;
    }

    stepsRemaining -= 1;
  }

  return sibling === currentSlide ? NO_SLIDE : sibling;
}

type KeydownCallbacks = {
  nextSlide: () => void,
  previousSlide: () => void,
  exit: () => void
};

function keyDownHandler(callbacks: KeydownCallbacks) {
  const { nextSlide, previousSlide, exit } = callbacks;

  return (event: KeyboardEvent) => {
    switch (event.key) {
      case 'ArrowRight': // falls thru
      case ' ': // Spacebar
        event.preventDefault();
        nextSlide();
        break;
      case 'ArrowLeft':
        event.preventDefault();
        previousSlide();
        break;
      case 'Escape':
        event.preventDefault();
        exit();
        break;
    }
  };
}

function changeCurrentSlideTo(slideContainer: HTMLElement) {
  /**
   * @param slideNumber 1-based slide number
   */
  return (slideNumber: number) => {
    const index = slideNumber - 1;
    const slide = slideContainer.children[index];

    if (slide instanceof HTMLElement) {
      showSlide(slide);
    }
  };
}

function changeCurrentSlideBy(slideContainer: HTMLElement) {
  return (delta: number) => {
    const currentSlide = getCurrent(slideContainer) ?? slideContainer.children[0];

    if (currentSlide instanceof HTMLElement) {
      const targetSlide = findSibling(currentSlide, delta);
      if (targetSlide !== NO_SLIDE) {
        showSlide(targetSlide);
      }
    }
  };
}

function showSlide(slide: HTMLElement) {
  scrollIntoView(slide);
}

function onPointerDown(element: HTMLElement, nextSlide: () => void) {
  /** Time of last down event */
  let downEventMillis = 0;

  return function handlePointerDown(downEvent: PointerEvent) {
    // Ignore down event if it happened in an interactible element
    const target = downEvent.target;
    if (target instanceof HTMLElement && target.closest('a[href], input, label, select, button')) {
      return;
    }

    // Remember time of down event to compare vs up event later
    downEventMillis = Date.now();

    function handlePointerUp() {
      const clickMaxMillis = 100;
      const pressDuration = Date.now() - downEventMillis;

      // It's a tap/click if it hasn't been too long since the "down" event
      if (pressDuration < clickMaxMillis) {
        nextSlide();
      }

      removeFollowOnListeners();
    }

    function handlePointerCancel() {
      removeFollowOnListeners();
    }

    function removeFollowOnListeners() {
      element.removeEventListener('pointerup', handlePointerUp);
      element.removeEventListener('pointercancel', handlePointerCancel);
    }

    element.addEventListener('pointerup', handlePointerUp);
    element.addEventListener('pointercancel', handlePointerCancel);
  };
}
