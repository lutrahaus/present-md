import { getSpellcheck, setSpellcheck } from '../localstorage';

export function set(editor: HTMLTextAreaElement, on: boolean) {
  editor.spellcheck = on;
  setSpellcheck(on);
}

export function value() {
  return getSpellcheck();
}
