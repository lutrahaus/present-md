export type KnownElements = ReturnType<typeof knownElements>;

/**
 * @returns DOM elements for the interactive parts of the UI.
 */
export function knownElements() {
  const main = document.getElementsByTagName('main')[0];
  if (!(main instanceof HTMLElement)) throw new Error('main element not found');

  const editor = document.getElementById('editor');
  if (!(editor instanceof HTMLTextAreaElement)) throw new Error('textarea element not found');

  const slideContainer = document.getElementById('slide-container');
  if (!(slideContainer instanceof HTMLDivElement)) throw new Error('#slide-container element not found');

  const politeRegion = document.getElementById('polite-announcer');
  if (!(politeRegion instanceof HTMLElement)) throw new Error('polite announcer element not found');

  const assertiveRegion = document.getElementById('assertive-announcer');
  if (!(assertiveRegion instanceof HTMLElement)) throw new Error('assertive announcer element not found');

  const slideshowButton = document.getElementById('slideshow-button');
  if (!(slideshowButton instanceof HTMLElement)) throw new Error('slideshow button not found');

  const spellcheckCheckbox = document.getElementById('spellcheck-checkbox');
  if (!(spellcheckCheckbox instanceof HTMLInputElement)) throw new Error('spellcheck checkbox not found');

  const shareButton = document.getElementById('share-button');
  if (!(shareButton instanceof HTMLButtonElement)) throw new Error('share button not found');

  const saveButton = document.getElementById('save-button');
  if (!(saveButton instanceof HTMLButtonElement)) throw new Error('save button not found');

  const openInput = document.getElementById('open-input');
  if (!(openInput instanceof HTMLInputElement)) throw new Error('open input not found');

  const slidesThemeLink = document.getElementById('slides-theme-link');
  if (!(slidesThemeLink instanceof HTMLLinkElement)) throw new Error('slides theme link not found');

  const syntaxThemeLink = document.getElementById('syntax-theme-link');
  if (!(syntaxThemeLink instanceof HTMLLinkElement)) throw new Error('syntax theme link not found');

  return {
    main,
    editor,
    slideContainer,
    politeRegion,
    assertiveRegion,
    slideshowButton,
    spellcheckCheckbox,
    shareButton,
    saveButton,
    openInput,
    slidesThemeLink,
    syntaxThemeLink,
  };
}
