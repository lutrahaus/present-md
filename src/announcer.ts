type Urgency = 'polite' | 'assertive';
type TimeoutId = ReturnType<typeof setTimeout>;

export type Announcer = ReturnType<typeof create>;

export function create(politeRegion: HTMLElement, assertiveRegion: HTMLElement) {
  politeRegionOn(politeRegion);
  assertiveRegionOn(assertiveRegion);

  document.addEventListener('visibilitychange', () => {
    // Turn off live regions when page not visible
    if (document.visibilityState === 'hidden') {
      liveRegionOff(politeRegion);
      liveRegionOff(assertiveRegion);
    } else {
      politeRegionOn(politeRegion);
      assertiveRegionOn(assertiveRegion);
    }
  });

  const createAnnouncer = () => {
    /** Timeouts used for announcement bookkeeping */
    const timeoutIds: Record<Urgency, Array<TimeoutId>> = {
      polite: [],
      assertive: []
    };

    return (liveRegion: HTMLElement, urgency: Urgency) => (message: string) => {
      // Clear any pending timeouts
      timeoutIds[urgency].forEach(timeoutId => clearTimeout(timeoutId));
      timeoutIds[urgency] = [];

      /*
       * Schedule new announcement for a bit later instead of setting it asap
       * because some screenreaders may not read the aria-live content if it
       * changes too fast.
       *
       * https://github.com/nvaccess/nvda/issues/8873
       */
      const showMessageDelayMillis = 150;
      const showMessageTimeoutId = setTimeout(
        () => liveRegion.textContent = message,
        showMessageDelayMillis
      );

      // Auto-expire announcement
      const expireMessageDelayMillis = 5000;
      const clearMessageTimeoutId = setTimeout(
        () => liveRegion.textContent = '',
        expireMessageDelayMillis
      );

      // Remember timeout ids to clear before next announcement
      timeoutIds[urgency] = [showMessageTimeoutId, clearMessageTimeoutId];
    }
  };

  const announce = createAnnouncer();

  return {
    announcePolite: announce(politeRegion, 'polite'),
    announceAssertive: announce(assertiveRegion, 'assertive'),
  };
}

function assertiveRegionOn(region: HTMLElement) {
  region.setAttribute('role', 'status');
  region.setAttribute('aria-live', 'assertive');
}

function politeRegionOn(region: HTMLElement) {
  region.setAttribute('role', 'status');
  region.setAttribute('aria-live', 'polite');
}

function liveRegionOff(region: HTMLElement) {
  region.setAttribute('role', 'none');
  region.setAttribute('aria-live', 'off');
}
