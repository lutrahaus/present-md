/** Valid values for "theme" frontmatter property. */
export const themes = ['system', 'dark', 'light'] as const;

export type Theme = typeof themes[number];
export const defaultTheme: Theme = 'system';

export const defaultHtml = false;

/** Config and metadata */
export type Frontmatter = {
  theme: Theme;
  /**
   * true if html in slide content is rendered, false if not.
   */
  html: boolean;
};
