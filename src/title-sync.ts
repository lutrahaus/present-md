import { title, type Deck } from "./deck";

export function syncDocumentTitle(deck: Deck) {
  window.document.title = `present-md | ${title(deck)}`
}
