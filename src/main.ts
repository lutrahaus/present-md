import { create as createSlideshow } from './toolbar/slideshow';
import { share } from './toolbar/share';
import { save } from './toolbar/save';
import * as spellcheck from './toolbar/spellcheck';
import { themeChanger } from './change-slides-theme';
import { create as createAnnouncer } from './announcer';
import { onDoubleClickSlide } from './utils/on-click-slide';
import { knownElements } from './known-elements';
import { initialize as initEditor } from './editor';
import { initialMarkdown } from './initial-markdown';
import { parseDeck } from './parse/parse-deck';
import { markFirstVisit, setMarkdown as syncLocalstorage } from './localstorage';
import { renderSlides } from './render/render-slides';
import { currentUrl } from './utils/url';
import { contentFromUrl, syncUrl } from './url-sync';
import { syncDocumentTitle } from './title-sync';
import { filename } from './deck';

import './style.css'

const {
  main,
  editor,
  slideContainer,
  politeRegion,
  assertiveRegion,
  slideshowButton,
  shareButton,
  saveButton,
  openInput,
  slidesThemeLink,
  syntaxThemeLink,
  spellcheckCheckbox,
} = knownElements();

const updateTheme = themeChanger(import.meta.env.BASE_URL, slideContainer, slidesThemeLink, syntaxThemeLink);

let markdown = initialMarkdown();
let deck = parseDeck(markdown);

const {
  overwriteValue: overwriteEditorValue,
  setValue: setEditorValue,
} = initEditor(editor, slideContainer, markdown, function onChange(md, source) {
  markdown = md;
  deck = parseDeck(md);

  // render preview
  renderSlides(deck, slideContainer);

  // other side effects
  updateTheme(deck.frontmatter.theme);
  syncDocumentTitle(deck);
  syncLocalstorage(md);
  syncUrl(md, source === 'initial' || source === 'url' || source === 'file');
});

const announcer = createAnnouncer(politeRegion, assertiveRegion);
const slideshow = createSlideshow(main, slideContainer, announcer);

// Save
saveButton.addEventListener('click', () => {
  save(filename(deck), markdown);
});

// Open
openInput.addEventListener('change', () => {
  if (openInput.files && openInput.files.length > 0) {
    const file = openInput.files[0];
    file.text().then(text => overwriteEditorValue(text, file.name, 'file'));
  }
});

// Share
shareButton.addEventListener('click', () => share(editor.value));

// Slideshow
slideshowButton.addEventListener('click', () => slideshow.start());

// Slideshow - mouse shortcut
onDoubleClickSlide(slideContainer, slide => {
  slideshow.start(Number(slide.dataset.slideNumber));
});

// Slideshow - keyboard shortcut
document.addEventListener('keydown', event => {
  if (event.altKey && event.key === 'Enter') {
    event.preventDefault();
    slideshow.start();
  }
});

// Spellcheck
spellcheck.set(editor, spellcheckCheckbox.checked = spellcheck.value());
spellcheckCheckbox.addEventListener('change', event => {
  if (event.target instanceof HTMLInputElement) {
    spellcheck.set(editor, event.target.checked);
  }
});

window.addEventListener('popstate', () => {
  setEditorValue(contentFromUrl(currentUrl()) || '', 'url');
});

markFirstVisit();
