import { prefersReducedMotion } from './utils/prefers-reduced-motion';
export function setChPosition(slideElement: HTMLElement, chPosition: number) {
  slideElement.dataset.chPosition = String(chPosition);
}

export function getChPosition(slideElement: HTMLElement): number | null {
  return slideElement.dataset.chPosition
    ? Number(slideElement.dataset.chPosition)
    : null;
}

/**
 * @param slideNumber 1-based
 */
export function setSlideNumber(slideElement: HTMLElement, slideNumber: number) {
  slideElement.dataset.slideNumber = String(slideNumber);
}

export function setHasCursor(slideElement: HTMLElement, hasCursor: boolean) {
  if (hasCursor) {
    slideElement.dataset.hasCursor = 'true';
  } else {
    delete slideElement.dataset.hasCursor;
  }
}

export function getHasCursor(slideElement: HTMLElement): boolean {
  return slideElement.dataset.hasCursor === 'true';
}

export function setContent(slideElement: HTMLElement, html: string) {
  slideElement.children[0].innerHTML = html;
}

export function blankSlideElement() {
  const slideElement = document.createElement('div');
  slideElement.classList.add('c-slide');

  const slideContentContainer = document.createElement('div')
  slideContentContainer.classList.add('c-slide__content');

  slideElement.appendChild(slideContentContainer);
  return slideElement;
}

/**
 * @param slideElement Element to scroll into view
 * @param optionsOverrides Scroll options
 */
export function scrollIntoView(slideElement: HTMLElement, optionsOverrides: ScrollIntoViewOptions = {}) {
  if (optionsOverrides.behavior === 'smooth') {
    throw new Error("Cannot override behavior to 'smooth'");
  }

  const options: ScrollIntoViewOptions = {
    behavior: prefersReducedMotion() ? 'auto' : 'smooth',
    ...optionsOverrides,
  }

  slideElement.scrollIntoView(options);
}
