import { Line } from '../deck';
import { themes, Frontmatter, defaultTheme } from '../frontmatter';

export function parseFrontmatter(lines: Array<Line>): Frontmatter {
  const keyValueRegex = /^(\w+):\s*(.+)\s*$/;

  const entries = lines
    .map(line => line.content.match(keyValueRegex))
    .map<[string, string] | null>(match => match ? [match[1], match[2]] : null)
    .filter(<T>(pair: T): pair is NonNullable<T> => !!pair)
    .map<[string, string]>(([key, value]) =>
      [key.toLocaleLowerCase(), value.toLocaleLowerCase()]);

  return normalizeFrontmatter(Object.fromEntries(entries));
}

function normalizeFrontmatter(values: {[k: string]: string}): Frontmatter {
  return {
    theme: themes.find((theme): theme is Frontmatter['theme'] => theme === values.theme) ?? defaultTheme,
    html: values.html === 'true'
  };
}
