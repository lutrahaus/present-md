import { Deck, Line } from '../deck';
import { parseFrontmatter } from './parse-frontmatter';

/** Line consisting of a hashtag followed by zero to many non-hashtags */
const slideStartPattern = /^#([^#]*?)$/;

export function parseDeck(markdown: string): Deck {
  const [preSlideLines, ...linesBySlide] = Array.from(splitBySlide(markdown));
  const frontmatter = parseFrontmatter(preSlideLines);

  return {
    frontmatter,
    slides: linesBySlide.map(lines => {
      const firstLine = lines[0].content;
      const firstLineMatch = slideStartPattern.exec(firstLine);

      return {
        // title is content of the first capture group
        title: firstLineMatch ? firstLineMatch[1].trim() : '',
        lines,
        chPosition: lines[0].chPosition,
        embeddedHtml: frontmatter.html ? 'render' : 'escape',
      };
    })
  };
}

function* splitBySlide(markdown: string) {
  let charCount = 0;
  let currentSlide: Array<Line> = [];

  for (const rawLine of markdown.split(/\n/)) {
    if (slideStartPattern.test(rawLine)) {
      yield currentSlide;
      currentSlide = [];
    }

    // Add one for newline that was dropped by String.prototype.split()
    const actualLineLength = rawLine.length + 1;

    currentSlide.push({
      content: rawLine,
      lineLength: actualLineLength,
      chPosition: charCount
    });

    charCount += actualLineLength;
  }

  yield currentSlide;
}
