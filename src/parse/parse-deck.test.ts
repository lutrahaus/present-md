import { describe, expect, test } from 'vitest';
import { parseDeck } from './parse-deck';

describe('parse deck', () => {
  test('one slide heading', () => {
    const input = '# First\nContent 1';
    const deck = parseDeck(input);

    expect(deck).toEqual({
      frontmatter: {
        theme: 'system',
        html: false,
      },
      slides: [
        {
          title: 'First',
          chPosition: 0,
          embeddedHtml: 'escape',
          lines: [
            {
              chPosition: 0,
              content: '# First',
              lineLength: 8
            },
            {
              chPosition: 8,
              content: 'Content 1',
              lineLength: 10
            }
          ]
        }
      ]
    });
  });

  test('two slide headings', () => {
    const input = '# First\nContent 1\n# Second';
    const deck = parseDeck(input);

    expect(deck).toEqual({
      frontmatter: {
        theme: 'system',
        html: false,
      },
      slides: [
        {
          title: 'First',
          chPosition: 0,
          embeddedHtml: 'escape',
          lines: [
            {
              chPosition: 0,
              content: '# First',
              lineLength: 8
            },
            {
              chPosition: 8,
              content: 'Content 1',
              lineLength: 10
            }
          ]
        },
        {
          title: 'Second',
          chPosition: 18,
          embeddedHtml: 'escape',
          lines: [
            {
              chPosition: 18,
              content: '# Second',
              lineLength: 9
            }
          ]
        }
      ]
    });
  });

  // Maybe this will be treated as frontmatter
  test('text before first slide heading', () => {
    const input = 'Before\n# First';
    const deck = parseDeck(input);

    expect(deck).toEqual({
      frontmatter: {
        theme: 'system',
        html: false,
      },
      slides: [
        {
          title: 'First',
          chPosition: 7,
          embeddedHtml: 'escape',
          lines: [
            {
              chPosition: 7,
              content: '# First',
              lineLength: 8
            }
          ]
        }
      ]
    });
  });

  test('only text', () => {
    const input = 'Blah';
    const deck = parseDeck(input);

    expect(deck).toEqual({
      frontmatter: {
        theme: 'system',
        html: false,
      },
      slides: []
    });
  });

  test('only whitespace', () => {
    const input = '    \n    ';
    const deck = parseDeck(input);

    expect(deck).toEqual({
      frontmatter: {
        theme: 'system',
        html: false,
      },
      slides: []
    });
  });

  test('only empty string', () => {
    const input = '';
    const deck = parseDeck(input);

    expect(deck).toEqual({
      frontmatter: {
        theme: 'system',
        html: false,
      },
      slides: []
    });
  });
});
